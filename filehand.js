const http = require('http')
const fs = require('fs')

const server = http.createServer((req,res) =>{
    if(req.method === 'GET'){
        let dsf = fs.readFileSync('./text.json','utf8')
        let json = JSON.parse(dsf)
        let data = JSON.stringify(json)
        console.log(data)
        fs.writeFileSync('./test.txt',data, err =>{
            if(err){
                console.log("Error occured in writing a file data")
            }
        })
        res.writeHead(200,{'Content-Type':'text/html'})
        res.write(data)
        res.end()
    }
})

server.listen(5000)
console.log("Server running at 5000")

